package strategy

import (
	"bitbucket.org/bin7se/nova/constant"
	"bitbucket.org/bin7se/nova/log"
	"bitbucket.org/bin7se/nova/trader"
	"github.com/markcheno/go-talib"
	"math"
	"time"
)

type Strategy interface {
	OnStart(ctx *trader.TradeContext)
	OnTick(ctx *trader.TradeContext)
	OnStop(ctx *trader.TradeContext)
}

type MacdObvStg struct {
	WarmUp time.Duration

	macd       map[string][]float64
	obv        map[string][]float64
	indicators []string
}

func (s *MacdObvStg) OnStart(ctx *trader.TradeContext) {
	s.macd = make(map[string][]float64)
	s.obv = make(map[string][]float64)
	s.indicators = []string{constant.K_3M, constant.K_15M, constant.K_30M, constant.K_1H}
}

func (s *MacdObvStg) OnTick(ctx *trader.TradeContext) {
	for _, v := range s.indicators {
		klines, ok := ctx.Klines[v]
		if !ok {
			continue
		}
		in := make([]float64, 0)
		inVolume := make([]float64, 0)
		for _, kline := range klines {
			in = append(in, kline.Close)
			inVolume = append(inVolume, kline.Volume)
		}
		if len(in) >= 26 {
			_, _, s.macd[v] = talib.Macd(in, 12, 26, 9)
		}
		s.obv[v] = talib.Obv(in, inVolume)
	}

	if ctx.Timestamp.After(ctx.StartTime.Add(s.WarmUp)) {
		macd3M := s.macd[constant.K_3M]
		obv3M := s.obv[constant.K_3M]

		macd15M := s.macd[constant.K_15M]
		obv15M := s.obv[constant.K_15M]

		macd30M := s.macd[constant.K_30M]
		obv30M := s.obv[constant.K_30M]

		if len(macd15M) <= 2 || len(obv15M) <= 2 || len(macd30M) <= 2 || len(obv30M) <= 2 {
			return
		}

		if diffX(obv15M[len(obv15M)-4], obv15M[len(obv15M)-3]) > 0 && diffX(macd15M[len(macd15M)-4], macd15M[len(macd15M)-3]) > 0 {
			if diffX(obv15M[len(obv15M)-3], obv15M[len(obv15M)-2]) > 0 && diffX(macd15M[len(macd15M)-3], macd15M[len(macd15M)-2]) > 0 {
				if diffX(macd15M[len(macd15M)-2], macd15M[len(macd15M)-1]) > 0 && diffX(obv15M[len(obv15M)-2], obv15M[len(obv15M)-1]) > 0 {
					if diffX(macd3M[len(macd3M)-3], macd3M[len(macd3M)-2]) > 0 && diffX(obv3M[len(obv3M)-3], obv3M[len(obv3M)-2]) > 0 {
						if diffX(macd3M[len(macd3M)-2], macd3M[len(macd3M)-1]) > 0 && diffX(obv3M[len(obv3M)-2], obv3M[len(obv3M)-1]) > 0 {
							if ctx.Buy(ctx.Price) {
								log.Info("buy signal 1")
								log.Info("%.8f %.8f %.8f", macd15M[len(macd15M)-3], macd15M[len(macd15M)-2], macd15M[len(macd15M)-1])
							}
						}
					}

				}
			}
		}

		if diffX(obv15M[len(obv15M)-4], obv15M[len(obv15M)-3]) > 0 && diffX(macd15M[len(macd15M)-4], macd15M[len(macd15M)-3]) > 0 {
			if diffX(obv15M[len(obv15M)-3], obv15M[len(obv15M)-2]) > 0 && diffX(macd15M[len(macd15M)-3], macd15M[len(macd15M)-2]) > 0 {
				if diffX(obv15M[len(obv15M)-2], obv15M[len(obv15M)-1]) > 0 && diffX(macd15M[len(macd15M)-2], macd15M[len(macd15M)-1]) > 0 {
					if diffX(macd3M[len(macd3M)-3], macd3M[len(macd3M)-2]) > 0 && diffX(obv3M[len(obv3M)-3], obv3M[len(obv3M)-2]) > 0 {
						if diffX(macd3M[len(macd3M)-2], macd3M[len(macd3M)-1]) > 0 && diffX(obv3M[len(obv3M)-2], obv3M[len(obv3M)-1]) > 0 {
							if ctx.Buy(ctx.Price) {
								log.Info("buy signal 2")
								log.Info("%.8f %.8f %.8f", macd15M[len(macd15M)-3], macd15M[len(macd15M)-2], macd15M[len(macd15M)-1])
							}
						}
					}
				}
			}
		}

		if diffX(obv15M[len(obv15M)-2], obv15M[len(obv15M)-1]) < 0 && diffX(macd15M[len(macd15M)-2], macd15M[len(macd15M)-1]) < 0 {
			if diffX(macd3M[len(macd3M)-3], macd3M[len(macd3M)-2]) < 0 && diffX(obv3M[len(obv3M)-3], obv3M[len(obv3M)-2]) < 0 {
				if diffX(macd3M[len(macd3M)-2], macd3M[len(macd3M)-1]) < 0 && diffX(obv3M[len(obv3M)-2], obv3M[len(obv3M)-1]) < 0 {
					if ctx.CurrentProfit() < 0 {
						if ctx.Sell(ctx.Price) {
							log.Info("sell signal 0")
						}
					}
				}
			}
		}

		if diffX(macd30M[len(macd30M)-3], macd30M[len(macd30M)-2]) < 0 && diffX(macd30M[len(macd30M)-2], macd30M[len(macd30M)-1]) < 0 {
			if diffX(macd15M[len(macd15M)-3], macd15M[len(macd15M)-2]) < 0 || diffX(obv15M[len(obv15M)-3], obv15M[len(obv15M)-2]) < 0 {
				if diffX(macd15M[len(macd15M)-2], macd15M[len(macd15M)-1]) < 0 && diffX(obv15M[len(obv15M)-2], obv15M[len(obv15M)-1]) < 0 {
					if ctx.Sell(ctx.Price) {
						log.Info("sell signal 1")
					}
				}
			}
		} else if diffX(macd30M[len(macd30M)-3], macd30M[len(macd30M)-2]) > 0 {
			if diffX(obv15M[len(obv15M)-3], obv15M[len(obv15M)-2]) < 0 && diffX(macd15M[len(macd15M)-3], macd15M[len(macd15M)-2]) < 0 {

				if diffX(macd3M[len(macd3M)-3], macd3M[len(macd3M)-2]) < 0 && diffX(obv3M[len(obv3M)-3], obv3M[len(obv3M)-2]) < 0 {
					if ctx.Sell(ctx.Price) {
						log.Info("sell signal 2")
					}
				} else if diffX(macd3M[len(macd3M)-2], macd3M[len(macd3M)-1]) < -0.2 && diffX(obv3M[len(obv3M)-2], obv3M[len(obv3M)-1]) < -0.2 {
					if ctx.Sell(ctx.Price) {
						log.Info("sell signal 3")
					}
				}
			} else if diffX(obv15M[len(obv15M)-2], obv15M[len(obv15M)-1]) < -0.10 {
				if ctx.Sell(ctx.Price) {
					log.Info("sell signal 4")
				}
			}
		}

		if false {
			if diffX(macd30M[len(macd30M)-2], macd30M[len(macd30M)-1]) > 0 && diffX(obv30M[len(obv30M)-2], obv30M[len(obv30M)-1]) >= 0 {
				if diffX(obv15M[len(obv15M)-3], obv15M[len(obv15M)-2]) >= 0 && diffX(macd15M[len(macd15M)-3], macd15M[len(macd15M)-2]) >= 0 {
					if diffX(macd15M[len(macd15M)-2], macd15M[len(macd15M)-1]) > 0 && diffX(obv15M[len(obv15M)-2], obv15M[len(obv15M)-1]) >= 0 {
						if diffX(obv3M[len(obv3M)-3], obv3M[len(obv3M)-2]) >= 0 {
							if diffX(macd3M[len(macd3M)-2], macd3M[len(macd3M)-1]) >= 0 && diffX(obv3M[len(obv3M)-2], obv3M[len(obv3M)-1]) >= 0 {
								if ctx.Buy(ctx.Price) {
									log.Info("buy signal 1")
								}
							}
						} else {
							if diffX(macd3M[len(macd3M)-2], macd3M[len(macd3M)-1]) >= 0.3 && diffX(obv3M[len(obv3M)-2], obv3M[len(obv3M)-1]) >= 0 {
								if ctx.Buy(ctx.Price) {
									log.Info("buy signal 2")
								}
							}
						}
					}
				} else {
					if diffX(macd15M[len(macd15M)-2], macd15M[len(macd15M)-1]) > 0.5 && diffX(obv15M[len(obv15M)-2], obv15M[len(obv15M)-1]) >= 0 {
						if diffX(obv3M[len(obv3M)-3], obv3M[len(obv3M)-2]) >= 0 {
							if diffX(macd3M[len(macd3M)-2], macd3M[len(macd3M)-1]) >= 0 && diffX(obv3M[len(obv3M)-2], obv3M[len(obv3M)-1]) >= 0 {
								if ctx.Buy(ctx.Price) {
									log.Info("buy signal 3")
								}
							}
						} else {
							if diffX(macd3M[len(macd3M)-2], macd3M[len(macd3M)-1]) >= 0.3 && diffX(obv3M[len(obv3M)-2], obv3M[len(obv3M)-1]) >= 0 {
								if ctx.Buy(ctx.Price) {
									log.Info("buy signal 4")
								}
							}
						}
					}
				}
			}

			if diffX(macd30M[len(macd30M)-3], macd30M[len(macd30M)-2]) >= 0 {
				if diffX(macd15M[len(macd15M)-3], macd15M[len(macd15M)-2]) <= 0 && diffX(obv15M[len(obv15M)-3], obv15M[len(obv15M)-2]) <= 0 {
					if diffX(macd15M[len(macd15M)-2], macd15M[len(macd15M)-1]) <= 0 && diffX(obv15M[len(obv15M)-2], obv15M[len(obv15M)-1]) <= 0 {
						if diffX(macd3M[len(macd3M)-3], macd3M[len(macd3M)-2]) <= 0.1 && diffX(obv3M[len(obv3M)-3], obv3M[len(obv3M)-2]) <= 0 {
							if diffX(macd3M[len(macd3M)-2], macd3M[len(macd3M)-1]) <= 0 && diffX(obv3M[len(obv3M)-2], obv3M[len(obv3M)-1]) <= 0 {
								if ctx.Sell(ctx.Price) {
									log.Info("sell signal 3")
								}
							}
						}
					}
				} else if diffX(macd15M[len(macd15M)-3], macd15M[len(macd15M)-2]) >= 0 {
					if diffX(macd15M[len(macd15M)-2], macd15M[len(macd15M)-1]) <= 0 && diffX(obv15M[len(obv15M)-2], obv15M[len(obv15M)-1]) <= 0 {
						if diffX(macd3M[len(macd3M)-2], macd3M[len(macd3M)-1]) <= 0 && diffX(obv3M[len(obv3M)-2], obv3M[len(obv3M)-1]) <= 0 {
							if ctx.Sell(ctx.Price) {
								log.Info("sell signal 4")
							}
						}
					}
				}
			} else {

			}

			if diffX(macd30M[len(macd30M)-3], macd30M[len(macd30M)-2]) >= 0 && diffX(macd30M[len(macd30M)-2], macd30M[len(macd30M)-1]) <= 0 {
				if diffX(obv30M[len(obv30M)-2], obv30M[len(obv30M)-1]) <= -0.05 { //todo change this param trade off
					if ctx.Sell(ctx.Price) {
						log.Info("sell signal 1")
					}
				}
			}

			if diffX(macd30M[len(macd30M)-3], macd30M[len(macd30M)-2]) >= 0 && diffX(obv30M[len(obv30M)-3], obv30M[len(obv30M)-2]) <= 0 {
				if diffX(macd30M[len(macd30M)-2], macd30M[len(macd30M)-1]) <= 0 {
					if diffX(obv30M[len(obv30M)-2], obv30M[len(obv30M)-1]) <= 0 {
						if ctx.Sell(ctx.Price) {
							log.Info("sell signal 2")
						}
					}
				}
			}
		}

	}
}

func diffX(x, y float64) float64 {
	return (y - x) / math.Abs(x)
}

func (s *MacdObvStg) OnStop(ctx *trader.TradeContext) {
	log.Info("total profit:%.3f", ctx.Profit*100)
}
