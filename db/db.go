package db

import (
	"bitbucket.org/bin7se/nova/log"
	"github.com/dgraph-io/badger"
	"github.com/zippoxer/bow"
	"github.com/zippoxer/bow/codec/json"
)

var DB *bow.DB

func init() {
	db, err := bow.Open("database",
		bow.SetCodec(json.Codec{}),
		bow.SetBadgerOptions(badger.DefaultOptions))

	if err != nil {
		log.Fatal("%v", err)
	}
	DB = db

}
