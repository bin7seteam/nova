package db

import "time"

type Trade struct {
	Id        string `bow:"key"`
	TradeId   int
	Price     float64
	Quantity  float64
	Timestamp time.Time
}
