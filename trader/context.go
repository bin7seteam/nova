package trader

import (
	"bitbucket.org/bin7se/nova/constant"
	"bitbucket.org/bin7se/nova/exchange/binance"
	"bitbucket.org/bin7se/nova/log"
	"strings"
	"time"
)

type Kline struct {
	OpenTime  time.Time
	Open      float64
	High      float64
	Low       float64
	Close     float64
	Volume    float64
	CloseTime time.Time
	Closed    bool
}

type TradeContext struct {
	Price         float64
	BuyPrice      float64
	Profit        float64
	StartTime     time.Time
	Timestamp     time.Time
	Klines        map[string][]*Kline
	requestKlines map[string][]*Kline

	Api binance.Binance
}

func (ctx *TradeContext) Buy(price float64) bool {
	if ctx.BuyPrice > 0 {
		return false
	}
	ctx.BuyPrice = price
	log.Info("time:%v\tbuy:%v", ctx.Timestamp, price)
	return true
}

func (ctx *TradeContext) Sell(price float64) bool {
	if ctx.BuyPrice == 0 {
		return false
	}
	profit := (price - ctx.BuyPrice) / ctx.BuyPrice
	ctx.Profit += profit
	log.Info("time:%v\tsell:%v\t profit:%.2f\t totalprofit:%.2f", ctx.Timestamp, price, profit*100, ctx.Profit*100)
	ctx.BuyPrice = 0
	return true
}

func (ctx *TradeContext) CurrentProfit() float64 {
	return (ctx.Price - ctx.BuyPrice) / ctx.BuyPrice
}

func (ctx *TradeContext) Init() {
	ctx.Klines = make(map[string][]*Kline)
	ctx.Klines[constant.K_1M] = make([]*Kline, 0)
	ctx.Klines[constant.K_3M] = make([]*Kline, 0)
	ctx.Klines[constant.K_5M] = make([]*Kline, 0)
	ctx.Klines[constant.K_15M] = make([]*Kline, 0)
	ctx.Klines[constant.K_30M] = make([]*Kline, 0)
	ctx.Klines[constant.K_1H] = make([]*Kline, 0)
	ctx.Klines[constant.K_2H] = make([]*Kline, 0)
	ctx.Klines[constant.K_4H] = make([]*Kline, 0)
	ctx.Klines[constant.K_6H] = make([]*Kline, 0)
	ctx.Klines[constant.K_12H] = make([]*Kline, 0)
	ctx.Klines[constant.K_DAY] = make([]*Kline, 0)

	ctx.requestKlines = make(map[string][]*Kline)
	ctx.requestKlines[constant.K_3M] = make([]*Kline, 0)
	ctx.requestKlines[constant.K_15M] = make([]*Kline, 0)
	ctx.requestKlines[constant.K_30M] = make([]*Kline, 0)
}

func (ctx *TradeContext) updateClosedKline(kline *Kline, interval int) {
	kline.Closed = true
	if interval == 3 {
		if len(ctx.requestKlines[constant.K_3M]) == 0 ||
			kline.OpenTime.After(ctx.requestKlines[constant.K_3M][len(ctx.requestKlines[constant.K_3M])-1].OpenTime) {
			//log.Debug("need request kline: %v", kline.OpenTime)
			res, err := ctx.Api.Klines(binance.KlinesRequest{
				Symbol:    "EOSUSDT",
				Interval:  binance.ThreeMinutes,
				Limit:     200,
				StartTime: kline.OpenTime.Add(-time.Second).UnixNano() / int64(time.Millisecond),
			})
			if err != nil {
				log.Error("request klines error %v", err)
			}
			ctx.requestKlines[constant.K_3M] = make([]*Kline, 0)
			for _, r := range res {
				ctx.requestKlines[constant.K_3M] = append(ctx.requestKlines[constant.K_3M], &Kline{
					OpenTime:  r.OpenTime,
					CloseTime: r.CloseTime,
					Open:      r.Open,
					Close:     r.Close,
					High:      r.High,
					Low:       r.Low,
					Volume:    r.Volume,
				})
				//log.Debug("request kline: %v", r.OpenTime)
			}
		}

		found := false
		for _, rk := range ctx.requestKlines[constant.K_3M] {
			if rk.OpenTime.Equal(kline.OpenTime) {
				found = true
				kline.Volume = rk.Volume
				kline.Open = rk.Open
				kline.Low = rk.Low
				kline.High = rk.High
				kline.Close = rk.Close
				break
			}
		}

		if !found {
			log.Error("not found request kline 3m %v", kline.OpenTime)
		}
	} else if interval == 15 {
		if len(ctx.requestKlines[constant.K_15M]) == 0 ||
			kline.OpenTime.After(ctx.requestKlines[constant.K_15M][len(ctx.requestKlines[constant.K_15M])-1].OpenTime) {
			//log.Debug("need request kline: %v", kline.OpenTime)
			res, err := ctx.Api.Klines(binance.KlinesRequest{
				Symbol:    "EOSUSDT",
				Interval:  binance.FifteenMinutes,
				Limit:     40,
				StartTime: kline.OpenTime.Add(-time.Second).UnixNano() / int64(time.Millisecond),
			})
			if err != nil {
				log.Error("request klines error %v", err)
			}
			ctx.requestKlines[constant.K_15M] = make([]*Kline, 0)
			for _, r := range res {
				ctx.requestKlines[constant.K_15M] = append(ctx.requestKlines[constant.K_15M], &Kline{
					OpenTime:  r.OpenTime,
					CloseTime: r.CloseTime,
					Open:      r.Open,
					Close:     r.Close,
					High:      r.High,
					Low:       r.Low,
					Volume:    r.Volume,
				})
				if strings.Contains(r.OpenTime.Format("2006-01-02 15:04:05"), "2018-09-02 12:30") {
					log.Debug("request kline: %v openg:%v close:%v", r.OpenTime, r.Open, r.Close)
				}
				//log.Debug("request kline: %v", r.OpenTime)
			}
		}

		found := false
		for _, rk := range ctx.requestKlines[constant.K_15M] {
			if rk.OpenTime.Equal(kline.OpenTime) {
				found = true
				kline.Volume = rk.Volume
				kline.Open = rk.Open
				kline.Low = rk.Low
				kline.High = rk.High
				kline.Close = rk.Close
				break
			}
		}

		if !found {
			log.Error("not found request kline  15m %v", kline.OpenTime)
		}
	} else if interval == 30 {
		if len(ctx.requestKlines[constant.K_30M]) == 0 ||
			kline.OpenTime.After(ctx.requestKlines[constant.K_30M][len(ctx.requestKlines[constant.K_30M])-1].OpenTime) {
			//log.Debug("need request kline: %v", kline.OpenTime)
			res, err := ctx.Api.Klines(binance.KlinesRequest{
				Symbol:    "EOSUSDT",
				Interval:  binance.ThirtyMinutes,
				Limit:     20,
				StartTime: kline.OpenTime.Add(-time.Second).UnixNano() / int64(time.Millisecond),
			})
			if err != nil {
				log.Error("request klines error %v", err)
			}
			ctx.requestKlines[constant.K_30M] = make([]*Kline, 0)
			for _, r := range res {
				ctx.requestKlines[constant.K_30M] = append(ctx.requestKlines[constant.K_30M], &Kline{
					OpenTime:  r.OpenTime,
					CloseTime: r.CloseTime,
					Open:      r.Open,
					Close:     r.Close,
					High:      r.High,
					Low:       r.Low,
					Volume:    r.Volume,
				})
				//log.Debug("request kline: %v", r.OpenTime)
			}
		}

		found := false
		for _, rk := range ctx.requestKlines[constant.K_30M] {
			if rk.OpenTime.Equal(kline.OpenTime) {
				found = true
				kline.Volume = rk.Volume
				kline.Open = rk.Open
				kline.Low = rk.Low
				kline.High = rk.High
				kline.Close = rk.Close
				break
			}
		}

		if !found {
			log.Error("not found request kline  30m %v", kline.OpenTime)
		}
	}
}

func (ctx *TradeContext) addKline(klines []*Kline, timestamp time.Time, interval int, price float64, quantity float64) []*Kline {
	if len(klines) > 0 {
		ctx.updateClosedKline(klines[len(klines)-1], interval)
	}
	kline := &Kline{
		OpenTime:  time.Date(timestamp.Year(), timestamp.Month(), timestamp.Day(), timestamp.Hour(), timestamp.Minute(), 0, 0, timestamp.Location()).Add(-time.Duration(timestamp.Minute() % interval) * time.Minute),
		CloseTime: time.Date(timestamp.Year(), timestamp.Month(), timestamp.Day(), timestamp.Hour(), timestamp.Minute(), 0, 0, timestamp.Location()).Add(-time.Duration(timestamp.Minute() % interval)*time.Minute + time.Duration(interval)*time.Minute - time.Microsecond),
		Open:      price,
		Close:     price,
		High:      price,
		Low:       price,
		Volume:    quantity,
	}

	if len(klines) > 0 {
		for klines[len(klines)-1].OpenTime.Add(time.Duration(interval) * time.Minute).Before(kline.OpenTime) {
			nopkline := &Kline{
				OpenTime:  klines[len(klines)-1].OpenTime.Add(time.Duration(interval) * time.Minute),
				CloseTime: klines[len(klines)-1].OpenTime.Add(time.Duration(interval) * time.Minute).Add(time.Duration(interval)*time.Minute - time.Microsecond),
				Open:      klines[len(klines)-1].Close,
				Close:     klines[len(klines)-1].Close,
				High:      klines[len(klines)-1].Close,
				Low:       klines[len(klines)-1].Close,
				Volume:    0,
				Closed:    true,
			}
			klines = append(klines, nopkline)
		}
	}

	klines = append(klines, kline)
	return klines
}

func (ctx *TradeContext) updateKline(klines []*Kline, price float64, quantity float64) {
	klines[len(klines)-1].Close = price
	if klines[len(klines)-1].High < price {
		klines[len(klines)-1].High = price
	}
	if klines[len(klines)-1].Low > price {
		klines[len(klines)-1].Low = price
	}
	klines[len(klines)-1].Volume += quantity
}

func (ctx *TradeContext) Update(price float64, quantity float64, timestamp time.Time) {
	ctx.Price = price
	ctx.Timestamp = timestamp
	if ctx.StartTime.IsZero() {
		ctx.StartTime = timestamp
	}

	if len(ctx.Klines[constant.K_1M]) == 0 || timestamp.After(ctx.Klines[constant.K_1M][len(ctx.Klines[constant.K_1M])-1].CloseTime) {
		ctx.Klines[constant.K_1M] = ctx.addKline(ctx.Klines[constant.K_1M], timestamp, 1, price, quantity)
	} else {
		ctx.updateKline(ctx.Klines[constant.K_1M], price, quantity)
	}

	if len(ctx.Klines[constant.K_3M]) == 0 || timestamp.After(ctx.Klines[constant.K_3M][len(ctx.Klines[constant.K_3M])-1].CloseTime) {
		ctx.Klines[constant.K_3M] = ctx.addKline(ctx.Klines[constant.K_3M], timestamp, 3, price, quantity)
	} else {
		ctx.updateKline(ctx.Klines[constant.K_3M], price, quantity)
	}

	if len(ctx.Klines[constant.K_5M]) == 0 || timestamp.After(ctx.Klines[constant.K_5M][len(ctx.Klines[constant.K_5M])-1].CloseTime) {
		ctx.Klines[constant.K_5M] = ctx.addKline(ctx.Klines[constant.K_5M], timestamp, 5, price, quantity)
	} else {
		ctx.updateKline(ctx.Klines[constant.K_5M], price, quantity)
	}

	if len(ctx.Klines[constant.K_15M]) == 0 || timestamp.After(ctx.Klines[constant.K_15M][len(ctx.Klines[constant.K_15M])-1].CloseTime) {
		ctx.Klines[constant.K_15M] = ctx.addKline(ctx.Klines[constant.K_15M], timestamp, 15, price, quantity)
	} else {
		ctx.updateKline(ctx.Klines[constant.K_15M], price, quantity)
	}

	if len(ctx.Klines[constant.K_30M]) == 0 || timestamp.After(ctx.Klines[constant.K_30M][len(ctx.Klines[constant.K_30M])-1].CloseTime) {
		ctx.Klines[constant.K_30M] = ctx.addKline(ctx.Klines[constant.K_30M], timestamp, 30, price, quantity)
	} else {
		ctx.updateKline(ctx.Klines[constant.K_30M], price, quantity)
	}

	if len(ctx.Klines[constant.K_1H]) == 0 || timestamp.After(ctx.Klines[constant.K_1H][len(ctx.Klines[constant.K_1H])-1].CloseTime) {
		ctx.Klines[constant.K_1H] = ctx.addKline(ctx.Klines[constant.K_1H], timestamp, 60, price, quantity)
	} else {
		ctx.updateKline(ctx.Klines[constant.K_1H], price, quantity)
	}

	if len(ctx.Klines[constant.K_2H]) == 0 || timestamp.After(ctx.Klines[constant.K_2H][len(ctx.Klines[constant.K_2H])-1].CloseTime) {
		ctx.Klines[constant.K_2H] = ctx.addKline(ctx.Klines[constant.K_2H], timestamp, 60*2, price, quantity)
	} else {
		ctx.updateKline(ctx.Klines[constant.K_2H], price, quantity)
	}

	if len(ctx.Klines[constant.K_4H]) == 0 || timestamp.After(ctx.Klines[constant.K_4H][len(ctx.Klines[constant.K_4H])-1].CloseTime) {
		ctx.Klines[constant.K_4H] = ctx.addKline(ctx.Klines[constant.K_4H], timestamp, 60*4, price, quantity)
	} else {
		ctx.updateKline(ctx.Klines[constant.K_4H], price, quantity)
	}

	if len(ctx.Klines[constant.K_6H]) == 0 || timestamp.After(ctx.Klines[constant.K_6H][len(ctx.Klines[constant.K_6H])-1].CloseTime) {
		ctx.Klines[constant.K_6H] = ctx.addKline(ctx.Klines[constant.K_6H], timestamp, 60*6, price, quantity)
	} else {
		ctx.updateKline(ctx.Klines[constant.K_6H], price, quantity)
	}

	if len(ctx.Klines[constant.K_12H]) == 0 || timestamp.After(ctx.Klines[constant.K_12H][len(ctx.Klines[constant.K_12H])-1].CloseTime) {
		ctx.Klines[constant.K_12H] = ctx.addKline(ctx.Klines[constant.K_12H], timestamp, 60*12, price, quantity)
	} else {
		ctx.updateKline(ctx.Klines[constant.K_12H], price, quantity)
	}

	if len(ctx.Klines[constant.K_DAY]) == 0 || timestamp.After(ctx.Klines[constant.K_DAY][len(ctx.Klines[constant.K_DAY])-1].CloseTime) {
		ctx.Klines[constant.K_DAY] = ctx.addKline(ctx.Klines[constant.K_DAY], timestamp, 60*24, price, quantity)
	} else {
		ctx.updateKline(ctx.Klines[constant.K_DAY], price, quantity)
	}
}
