package constant

const (
	K_1M  = "1M"
	K_3M  = "3M"
	K_5M  = "5M"
	K_15M = "15M"
	K_30M = "30M"
	K_1H  = "1H"
	K_2H  = "2H"
	K_4H  = "4H"
	K_6H  = "6H"
	K_12H = "12H"
	K_DAY = "DAY"
)
