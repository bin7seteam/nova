module bitbucket.org/bin7se/nova

require (
	github.com/AndreasBriese/bbloom v0.0.0-20170702084017-28f7e881ca57 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/deckarep/golang-set v0.0.0-20180831180637-cbaa98ba5575 // indirect
	github.com/dgraph-io/badger v1.5.3
	github.com/dgryski/go-farm v0.0.0-20180109070241-2de33835d102 // indirect
	github.com/golang/protobuf v1.2.0 // indirect
	github.com/gorilla/websocket v1.4.0
	github.com/markcheno/go-talib v0.0.0-20180811141714-1863983d486d
	github.com/pkg/errors v0.8.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sony/sonyflake v0.0.0-20160530021500-fa881fb1052b // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	github.com/zippoxer/bow v0.0.0-20180526182845-86bb0c3dbea0
	golang.org/x/net v0.0.0-20180826012351-8a410e7b638d // indirect
	golang.org/x/sync v0.0.0-20180314180146-1d60e4601c6f // indirect
	golang.org/x/sys v0.0.0-20180903190138-2b024373dcd9 // indirect
)
