package utils

import (
	"fmt"
)

// Provides leaky buffer, based on the example in Effective Go.

type LeakyBuf struct {
	Buf  []byte
	Size int
}

type LeakyBufPool struct {
	poolSize int
	bufSize  int // size of each buffer
	freeList chan *LeakyBuf
}

const LeakyBufSize = 4200 // data.len(2) + hmacsha1(10) + data(4096)
const MaxNBuf = 1024 * 25 // 100MB

// NewLeakyBuf creates a leaky buffer which can hold at most n buffer, each
// with bufSize bytes.
func NewLeakyBufPool(poolSize, bufSize int) *LeakyBufPool {
	return &LeakyBufPool{
		poolSize: poolSize,
		bufSize:  bufSize,
		freeList: make(chan *LeakyBuf, poolSize),
	}
}

// Get returns a buffer from the leaky buffer or create a new buffer.
func (lbp *LeakyBufPool) Get() (b *LeakyBuf) {
	select {
	case b = <-lbp.freeList:
	default:
		b = &LeakyBuf{
			Buf:  make([]byte, lbp.bufSize),
			Size: 0,
		}
	}
	b.Size = 0
	return
}

// Put add the buffer into the free buffer pool for reuse. Panic if the buffer
// size is not the same with the leaky buffer's. This is intended to expose
// error usage of leaky buffer.
func (lbp *LeakyBufPool) Put(b *LeakyBuf) {
	if len(lbp.freeList) >= lbp.poolSize-2000 {
		return
	}
	if len(b.Buf) != lbp.bufSize {
		fmt.Println("invalid buffer size that's put into leaky buffer")
		return
	}
	b.Size = 0
	lbp.freeList <- b
	return
}
