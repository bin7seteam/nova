package log

import (
	"bitbucket.org/bin7se/nova/utils"
	"fmt"
	"os"
	"path"
	"runtime"
	"sync"
	"time"
)

const (
	LevelTrace = iota
	LevelDebug
	LevelInfo
	LevelWarn
	LevelError
	LevelFatal
)

var defaultLogger Logger

func Debug(format string, args ...interface{}) {
	if defaultLogger != nil {
		defaultLogger.Output(2, LevelDebug, format, args...)
	}
}

func Info(format string, args ...interface{}) {
	if defaultLogger != nil {
		defaultLogger.Output(2, LevelInfo, format, args...)
	}
}

func Warn(format string, args ...interface{}) {
	if defaultLogger != nil {
		defaultLogger.Output(2, LevelWarn, format, args...)
	}
}

func Error(format string, args ...interface{}) {
	if defaultLogger != nil {
		defaultLogger.Output(2, LevelError, format, args...)
	}
}

func Fatal(format string, args ...interface{}) {
	if defaultLogger != nil {
		defaultLogger.Output(2, LevelFatal, format, args...)
	}
}

func Close() {
	if defaultLogger != nil {
		defaultLogger.Close()
	}
}

const (
	// Bits or'ed together to control what's printed.
	// There is no control over the order they appear (the order listed
	// here) or the format they present (as described in the comments).
	// The prefix is followed by a colon only when Llongfile or Lshortfile
	// is specified.
	// For example, flags Ldate | Ltime (or LstdFlags) produce,
	//	2009/01/23 01:23:23 message
	// while flags Ldate | Ltime | Lmicroseconds | Llongfile produce,
	//	2009/01/23 01:23:23.123123 /a/b/c/d.go:23: message
	Ldate         = 1 << iota     // the date in the local time zone: 2009/01/23
	Ltime                         // the time in the local time zone: 01:23:23
	Lmicroseconds                 // microsecond resolution: 01:23:23.123123.  assumes Ltime.
	Llongfile                     // full file name and line number: /a/b/c/d.go:23
	Lshortfile                    // final file name element and line number: d.go:23. overrides Llongfile
	LUTC                          // if Ldate or Ltime is set, use UTC rather than the local time zone
	LstdFlags     = Ldate | Ltime // initial values for the standard logger
)

var LevelName [6]string = [6]string{"Trace", "Debug", "Info", "Warn", "Error", "Fatal"}

const logBufferLen = 4096

const TimeFormat = "2006/01/02 15:04:05"

type Logger interface {
	Debug(format string, args ...interface{})
	Info(format string, args ...interface{})
	Warn(format string, args ...interface{})
	Error(format string, args ...interface{})
	Fatal(format string, args ...interface{})
	Output(callDepth int, level int, format string, v ...interface{})
	Close()
}

type logger struct {
	level   int
	flag    int
	mutex   *sync.RWMutex
	handler LoggerHandler
	quit    chan byte
	msg     chan *utils.LeakyBuf
	bufPool *utils.LeakyBufPool
}

func Init(level int, flag int, handler LoggerHandler) (Logger) {
	var l = new(logger)
	l.level = level
	l.flag = flag
	l.mutex = new(sync.RWMutex)
	l.handler = handler
	l.quit = make(chan byte)
	l.msg = make(chan *utils.LeakyBuf, 1024)
	l.bufPool = utils.NewLeakyBufPool(1024, logBufferLen)
	go l.run()
	defaultLogger = l
	return l
}

func (l *logger) run() {
	var buf *utils.LeakyBuf
	for {
		select {
		case buf = <-l.msg:
			l.handler.Write(buf.Buf[:buf.Size])
			l.bufPool.Put(buf)
		case <-l.quit:
			l.quit = nil
			//l.handler.Close()
			return
		}
	}
}

func (l *logger) Close() {
	if l.quit == nil {
		return
	}
	defer func() {
		if err := recover(); err != nil {

		}
	}()
	close(l.quit)
	var buf *utils.LeakyBuf
	for {
		select {
		case buf = <-l.msg:
			l.handler.Write(buf.Buf[:buf.Size])
			l.bufPool.Put(buf)
		case <-time.After(time.Duration(50) * time.Microsecond):
			l.handler.Close()
			return
		}
	}
}

// Cheap integer to fixed-width decimal ASCII.  Give a negative width to avoid zero-padding.
func itoa(buf []byte, i int, wid int) ([]byte) {
	// Assemble decimal in reverse order.
	var b [20]byte
	bp := len(b) - 1
	for i >= 10 || wid > 1 {
		wid--
		q := i / 10
		b[bp] = byte('0' + i - q*10)
		bp--
		i = q
	}
	// i < 10
	b[bp] = byte('0' + i)
	buf = append(buf, b[bp:]...)
	return buf
}

func (l *logger) Output(callDepth int, level int, format string, v ...interface{}) {
	if l.level > level {
		return
	}

	leakyBuf := l.bufPool.Get()
	buf := leakyBuf.Buf[0:0]

	t := time.Now()
	if l.flag&LUTC != 0 {
		t = t.UTC()
	}

	if l.flag&(Ldate|Ltime|Lmicroseconds) != 0 {
		if l.flag&Ldate != 0 {
			year, month, day := t.Date()
			buf = itoa(buf, year, 4)
			buf = append(buf, '-')
			buf = itoa(buf, int(month), 2)
			buf = append(buf, '-')
			buf = itoa(buf, day, 2)
			buf = append(buf, ' ')
		}
		if l.flag&(Ltime|Lmicroseconds) != 0 {
			hour, min, sec := t.Clock()
			buf = itoa(buf, hour, 2)
			buf = append(buf, ':')
			buf = itoa(buf, min, 2)
			buf = append(buf, ':')
			buf = itoa(buf, sec, 2)
			if l.flag&Lmicroseconds != 0 {
				buf = append(buf, '.')
				buf = itoa(buf, t.Nanosecond()/1e3, 6)
			}
			buf = append(buf, ' ')
		}
	}

	if l.flag&(Lshortfile|Llongfile) != 0 {
		_, file, line, ok := runtime.Caller(callDepth)
		if !ok {
			file = "???"
			line = 0
		} else {
			for i := len(file) - 1; i > 0; i-- {
				if file[i] == '/' {
					file = file[i+1:]
					break
				}
			}
		}

		if l.flag&Lshortfile != 0 {
			short := file
			for i := len(file) - 1; i > 0; i-- {
				if file[i] == '/' {
					short = file[i+1:]
					break
				}
			}
			file = short
		}
		buf = append(buf, file...)
		buf = append(buf, ':')
		buf = itoa(buf, line, -1)
	}

	buf = append(buf, " ["...)
	buf = append(buf, LevelName[level]...)
	buf = append(buf, "] "...)

	s := fmt.Sprintf(format, v...)
	if len(s)+len(buf) >= logBufferLen {
		s = s[:logBufferLen-len(buf)-10]
	}
	buf = append(buf, s...)

	if s[len(s)-1] != '\n' {
		buf = append(buf, '\n')
	}
	leakyBuf.Size = len(buf)
	l.msg <- leakyBuf

}

//LoggerHandler writes logs to somewhere
type LoggerHandler interface {
	Write(p []byte) (n int, err error)
	Close() error
}

type RotatingFileHandler struct {
	fd *os.File

	fileName    string
	maxBytes    int
	backupCount int
}

func NewRotatingFileHandler(fileName string, maxBytes int, backupCount int) (*RotatingFileHandler, error) {
	dir := path.Dir(fileName)
	os.Mkdir(dir, 0777)

	h := new(RotatingFileHandler)

	if maxBytes <= 0 {
		return nil, fmt.Errorf("invalid max bytes")
	}

	h.fileName = fileName
	h.maxBytes = maxBytes
	h.backupCount = backupCount

	var err error
	h.fd, err = os.OpenFile(fileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		return nil, err
	}

	return h, nil
}

func (h *RotatingFileHandler) Write(p []byte) (n int, err error) {
	h.doRollover()
	return h.fd.Write(p)
}

func (h *RotatingFileHandler) Close() error {
	if h.fd != nil {
		return h.fd.Close()
	}
	return nil
}

func (h *RotatingFileHandler) doRollover() {
	f, err := h.fd.Stat()
	if err != nil {
		return
	}

	if h.maxBytes <= 0 {
		return
	} else if f.Size() < int64(h.maxBytes) {
		return
	}

	if h.backupCount > 0 {
		h.fd.Close()

		for i := h.backupCount - 1; i > 0; i-- {
			sfn := fmt.Sprintf("%s.%d", h.fileName, i)
			dfn := fmt.Sprintf("%s.%d", h.fileName, i+1)

			os.Rename(sfn, dfn)
		}

		dfn := fmt.Sprintf("%s.1", h.fileName)
		os.Rename(h.fileName, dfn)

		h.fd, _ = os.OpenFile(h.fileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	}
}

//log with Trace level
func (l *logger) Trace(format string, v ...interface{}) {
	l.Output(2, LevelTrace, format, v...)
}

//log with Debug level
func (l *logger) Debug(format string, v ...interface{}) {
	l.Output(2, LevelDebug, format, v...)
}

//log with info level
func (l *logger) Info(format string, v ...interface{}) {
	l.Output(2, LevelInfo, format, v...)
}

//log with warn level
func (l *logger) Warn(format string, v ...interface{}) {
	l.Output(2, LevelWarn, format, v...)
}

//log with error level
func (l *logger) Error(format string, v ...interface{}) {
	l.Output(2, LevelError, format, v...)
}

//log with fatal level
func (l *logger) Fatal(format string, v ...interface{}) {
	l.Output(2, LevelFatal, format, v...)
	time.Sleep(100 * time.Millisecond)
	os.Exit(1)
}
