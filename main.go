package main

import (
	"bitbucket.org/bin7se/nova/backtest"
	"bitbucket.org/bin7se/nova/db"
	"bitbucket.org/bin7se/nova/exchange/binance"
	"bitbucket.org/bin7se/nova/log"
	"bitbucket.org/bin7se/nova/strategy"
	"bitbucket.org/bin7se/nova/trader"
	"context"
	"fmt"
	"os"
	"strconv"
	"time"
)

func main() {
	log.Init(log.LevelDebug, log.Ldate|log.Ltime|log.Lmicroseconds|log.Lshortfile, os.Stdout)

	hmacSigner := &binance.HmacSigner{
		Key: []byte(os.Getenv("BINANCE_SECRET")),
	}
	ctx, _ := context.WithCancel(context.Background())
	// use second return value for cancelling request
	binanceService := binance.NewAPIService(
		"https://www.binance.com",
		"",
		hmacSigner,
		ctx,
	)
	b := binance.NewBinance(binanceService)

	tradeCtx := &trader.TradeContext{
		Api: b,
	}
	tradeCtx.Init()
	st := strategy.MacdObvStg{
		WarmUp: time.Hour * 44,
	}
	backtest.BackTest(tradeCtx, time.Now().Add(-time.Hour*24*5-72*time.Hour), time.Now(), &st)

	time.Sleep(time.Second)
	return

	ex, err := b.ExchangeInfo()
	if err != nil {
		panic(err)
	}

	fmt.Printf("%#v\n", ex)

	ftime, err := time.Parse("2006-01-02 15:04:05", "2018-09-02 04:44:05")
	res, err := b.Klines(binance.KlinesRequest{
		Symbol:    "EOSUSDT",
		Interval:  binance.FifteenMinutes,
		Limit:     1,
		StartTime: ftime.UnixNano() / int64(time.Millisecond),
	})
	fmt.Println(len(res), *res[0], ftime)
	return

	var trade db.Trade
	startID := int64(6298700)
	for i := 0; i < 200; i++ {
		ts, err := b.AggTrades(binance.AggTradesRequest{
			Symbol: "EOSUSDT",
			FromID: startID,
			Limit:  1000,
		})

		if err != nil {
			log.Error("request error %v", err)
		}

		s := time.Now()
		if len(ts) == 0 {
			break
		}
		for _, t := range ts {
			trade.TradeId = t.ID
			trade.Quantity = t.Quantity
			trade.Price = t.Price
			trade.Timestamp = t.Timestamp
			trade.Id = t.Timestamp.Format("2006-01-02 15:04:05") + "_" + strconv.Itoa(t.ID)
			err := db.DB.Bucket("trades").Put(trade)
			if err != nil {
				log.Error("insert db error %v", err)
			}
		}
		log.Debug("%v %v", time.Since(s), trade)
		startID = int64(trade.TradeId)
		time.Sleep(time.Millisecond * 300)
	}
	db.DB.Close()
	log.Debug("insert over")

}
