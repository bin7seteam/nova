package backtest

import (
	"bitbucket.org/bin7se/nova/db"
	"bitbucket.org/bin7se/nova/log"
	"bitbucket.org/bin7se/nova/strategy"
	"bitbucket.org/bin7se/nova/trader"
	"fmt"
	"github.com/zippoxer/bow"
	"time"
)

func iterTrades(iter *bow.Iter, t *db.Trade) (b bool) {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("iter error", err)
		}
	}()
	iter.Next(t)
	b = true
	return
}

func BackTest(ctx *trader.TradeContext, from, to time.Time, st strategy.Strategy) {
	iter := db.DB.Bucket("trades").Iter()
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("backtest ", err)
		}
		iter.Close()
	}()
	st.OnStart(ctx)
	var trade db.Trade
	var tradeId int
	for iterTrades(iter, &trade) {
		if trade.Timestamp.After(from) && trade.Timestamp.Before(to) {
			if tradeId != 0 && trade.TradeId != tradeId+1 {
				log.Error("tradeid error, expect %v got %v", tradeId+1, trade.TradeId)
			}
			tradeId = trade.TradeId
			ctx.Update(trade.Price, trade.Quantity, trade.Timestamp)
			st.OnTick(ctx)
		}

	}
	st.OnStop(ctx)
	log.Info("backtest over")
}
